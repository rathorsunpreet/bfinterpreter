// This file is part of BFInterpreter.
// Copyright (C) 2017  Sunpreet Singh Rathor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Main.java
 * This file is the starting point for the program.
 * Created by: Sunpreet Singh Rathor
 */


import mode.DebugMode;
import mode.FileMode;
import mode.REPLMode;

public class Main {

    private FileMode fm;
    private REPLMode rm;
    private DebugMode dm;

    // Method to provide a summary on what options are available
    private void provideInfo() {
        System.out.println("BrainFuck Interpreter v1.0");
        System.out.println("Made by Sunpreet Singh Rathor");
        System.out.println("-----------------------------");
        System.out.println("Usage: java -jar bfinterpreter.jar [options] [filename]");
        System.out.println("where [] signifies optional.");
        System.out.println("-----------------------------");
        System.out.println("Options:");
        System.out.println("--h/--help: For printing this help.");
        System.out.println("--r/--repl-mode: For a REPL style interpreter.");
        System.out.println("--f/--file: Specify a full filename after this option.");
        System.out.println("--d/--debug-mode: Specify a full filename after this option and debug the program.");
        System.out.println("-----------------------------");
        System.out.println("Note: ");
        System.out.println("File should be a text file.");
        System.out.println("Type zero (0) to quit when providing input.");
    }

    // This is the method where the command line argument is parsed and appropriate modes are initialized
    public void parseArguments(String[] options) {
        if(options.length == 1) {
            if (options[0].equalsIgnoreCase("--h") || options[0].equalsIgnoreCase("--help")) {
                // Provide info on program
                provideInfo();
            } else if (options[0].equalsIgnoreCase("--r") || options[0].equalsIgnoreCase("--repl-mode")) {
                // To make REPL style interactive mode
                System.out.println("BFInterpreter  Copyright (C) 2017  Sunpreet Singh Rathor\n" +
                        "This program comes with ABSOLUTELY NO WARRANTY; for details type `show_w'.\n" +
                        "This is free software, and you are welcome to redistribute it\n" +
                        "under certain conditions; type `show_c' for details.");
                System.out.println("");
                System.out.println("BrainFuck Interpreter v1.0");
                System.out.println("Made by Sunpreet Singh Rathor");
                System.out.println("-----------------------------");
                System.out.println("REPL Mode Selected, type exit (any variation) or zero (0) during Input to exit program");

                rm = new REPLMode();
                rm.startMode();
            }
        } else {
            if (options[0].equalsIgnoreCase("--f") || options[0].equalsIgnoreCase("--file-mode")) {
                // To make File Mode
                fm = new FileMode();
                fm.setFile(options[1]);
                fm.startMode();
            }
            if(options[0].equalsIgnoreCase("--d") || options[0].equalsIgnoreCase("--debug-mode")) {
                // To make the Debug mode
                System.out.println("BFInterpreter  Copyright (C) 2017  Sunpreet Singh Rathor\n" +
                        "This program comes with ABSOLUTELY NO WARRANTY; for details type `show_w'.\n" +
                        "This is free software, and you are welcome to redistribute it\n" +
                        "under certain conditions; type `show_c' for details.");
                System.out.println("");
                System.out.println("BrainFuck Interpreter v1.0");
                System.out.println("Made by Sunpreet Singh Rathor");
                System.out.println("-----------------------------");
                System.out.println("Debug Mode Selected, type exit (any variation) during Input to exit program");

                dm = new DebugMode();
                dm.setFile(options[1]);
                dm.startMode();
            }
        }
    }

    public static void main(String[] args) {
        Main c = new Main();
        if(args.length == 0) {
            System.out.print("Use --h or --help for correct usage.");
            System.exit(0);
        } else if (args.length > 0) {
            c.parseArguments(args);
        }
    }
}
