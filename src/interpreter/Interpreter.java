// This file is part of BFInterpreter.
// Copyright (C) 2017  Sunpreet Singh Rathor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File: Interpreter.java
 * This is the file with the main method. The interpretation of BrainFuck syntax is implemented in this file.
 * Created by: Sunpreet Singh Rathor
 */

package interpreter;


import handler.UserHandler;

import java.util.Arrays;
import java.util.Stack;


public class Interpreter {

    private byte[] cells; // Memory for the interpreter to change and use
    private int ptr;    // Pointer to cells

    private byte[] code; // Program Code
    private int i;  // Pointer to code

    // Stack to store the positions of 91
    private Stack<Integer> c91;

    private UserHandler uh;

    private boolean isInLoop;   // True if in loop false otherwise


    public Interpreter(UserHandler u) {
        cells = new byte[30000];
        ptr = 0;
        c91 = new Stack<Integer>();
        code = null;
        uh = u;

        Arrays.fill(cells,(byte)0);
        i = 0;
        isInLoop = false;
    }

    public void setCode(byte[] c) {
        code = c;
    }

    public boolean checkIfInLoop() {
        return !c91.empty();
    }

    public int getI() {
        return i;
    }

    public void setCurrentTapeValue(byte c) {
        code[ptr] = c;
    }

    public boolean getIsInLoop() {
        return isInLoop;
    }

    // Method to wrap pointer to cells array
    private void setPointer() {
        if(ptr > 30000)
            ptr = ptr - 30000;
        if(ptr < 0)
            ptr = 30000 + ptr;
    }

    // Reset all values
    public void reset() {
        // Reset cells
        Arrays.fill(cells, (byte) 0);

        // Reset c91
        c91.clear();

        // Refill code
        if(code != null)
            Arrays.fill(code, (byte)0);

        // Reset ptr
        ptr = 0;

        // Set isInLoop to false
        isInLoop = false;
    }
    // This is the method where the interpreter decides what action to take based on the value of the byte c
    public void applyRules(byte c) {
        switch (c) {
            case 43: // Plus
                cells[ptr]++;
                break;
            case 44: //Comma
                byte check = 125;
                check = uh.getFirstByte("\nInput: ");
                if (check == 48)
                    System.exit(0);
                else
                    cells[ptr] = check;
                check = 0;
                break;
            case 45: // Minus
                cells[ptr]--;
                break;
            case 46: // Dot
                uh.displayByteAsChar(cells[ptr]);
                break;
            case 48: System.exit(0);
            case 60: // <
                ptr--;
                setPointer();
                break;
            case 62: // >
                ptr++;
                setPointer();
                break;
            case 91: // [
                c91.push((Integer)i);
                break;
            case 93: // ]
                if (cells[ptr] != 0) {
                    i = c91.lastElement();
                    isInLoop = true;
                } else {
                    c91.pop();
                    isInLoop = false;
                }
                break;
            default:
                break;
        }
    }

    // Method to iterate over the variable code while applying Brainfuck rules
    public void processCode() {
        for(i = 0; i < code.length; i++) {
            applyRules(code[i]);
        }
    }

    // Method to display the non-zero cells
    public void showCells() {
        int i = 0;
        uh.displayString("Cells location - Byte Data\n");
        uh.displayString("------------------------------\n");
        while(i <= code.length) {
            if(cells[i] != 0) {
                uh.displayString(String.valueOf(i) + " - ");
                uh.displayString(cells[i] + "");
                uh.displayString("\n");
            }
            i++;
        }
    }
}
