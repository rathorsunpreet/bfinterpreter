// This file is part of BFInterpreter.
// Copyright (C) 2017  Sunpreet Singh Rathor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File: UserHandler.java
 * This file contains all methods required to interact with user.
 * Created by: Sunpreet Singh Rathor
 */

package handler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserHandler {

    // Method to get user input, only returns first byte of input
    // no matter how big the string is.
    public byte getFirstByte(String prompt) {
        String input = null;

        InputStreamReader ir = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(ir);

        System.out.print(prompt);
        try {
            input = br.readLine();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return input.getBytes()[0];
    }

    // Method to get user input, returns the string as a byte array
    public byte[] getByteArray(String prompt) {
        String input = null;

        InputStreamReader ir = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(ir);

        System.out.print(prompt);
        try {
            input = br.readLine();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return input.getBytes();
    }

    // Method t get user input as a string
    public String getString(String prompt) {
        String input = null;

        InputStreamReader ir = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(ir);

        System.out.print(prompt);
        try {
            input = br.readLine();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return input;
    }

    // Method to display byte to console as a character
    public void displayByteAsChar(byte b) {
        System.out.print(new String(new byte[] {b}));
    }

    // Method to display string on console
    public void displayString(String s) {
        System.out.print(s);
    }

    // Method to display string on console with newline
    public void displayStringNLine(String s) {
        System.out.println(s);
    }

}
