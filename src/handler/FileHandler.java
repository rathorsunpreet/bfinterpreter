// This file is part of BFInterpreter.
// Copyright (C) 2017  Sunpreet Singh Rathor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * FileHandler.java
 * This file has control over reading and writing to files on the disk
 * Created by: Sunpreet Singh Rathor
 */

package handler;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class FileHandler {

    private File fil;
    private Scanner in;

    public FileHandler() {
        fil = null;
        in = null;
    }

    // Method to setup member variables for printing copyright
    public boolean setup() {
        if(checkIfFile("COPYING")) {
            fil = new File("COPYING");
            try {
                in = new Scanner(fil);
            } catch (FileNotFoundException fnfe) {
                fnfe.printStackTrace();
            }
            return true;
        } else {
            return false;
        }
    }

    // Method to close the file COPYING
    public void closeFile() {
        in.close();
    }

    // Method to check if EOF has been reached
    public boolean hasNext() {
        return in.hasNext();
    }

    // Method to read a copyright into a string
    public String getCopyright() {
        return in.nextLine();
    }

    // Method to read a file and return it as a byte array
    public byte[] getByteArray(String fname) {
        fil = new File(fname);
        FileInputStream fis = null;
        byte[] b = new byte[(int) fil.length()];

        try {
            fis = new FileInputStream(fil);
            fis.read(b);
            fis.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return b;
    }

    // Method to write ArrayList to specified file
    public void writeArrayList(ArrayList<String> lst, String fname) {
        FileWriter fw = null;
        try {
            if(fname == null) {
                fil = new File("history.txt");
                fw = new FileWriter(fil);
            } else {
                fil = new File(fname);
                fw = new FileWriter(fil);
            }
            for (String s: lst) {
                fw.write(s + "\n");
            }
            fw.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Method to read ArrayList from file history.txt
    public ArrayList<String> readArrayList(String fname) {
        Scanner s = null;
        ArrayList<String> retVal = new ArrayList<String>();
        try {
            if(fname == null) {
                fil = new File("history.txt");
            } else {
                fil = new File(fname);
            }
            s = new Scanner(fil);
            while(s.hasNext()) {
                retVal.add(s.next());
            }
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return retVal;
    }

    // Checks if file exists on hard disk
    public boolean checkIfFile(String fname) {
        if(fname == null)
            fname = "history.txt";
        fil = new File(fname);
        return (fil.exists() && fil.isFile());
    }

    // Method to delete a file
    public boolean deleteFile(String fname) {
        if(fname == null) {
            fil = new File("history.txt");
            return fil.delete();
        } else {
            fil = new File(fname);
            return fil.delete();
        }
    }
}
