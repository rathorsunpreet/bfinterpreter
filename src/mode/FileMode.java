// This file is part of BFInterpreter.
// Copyright (C) 2017  Sunpreet Singh Rathor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * FileMode.java
 * This is where the code written on a file can be fed to the interpeter for processing.
 * Created by: Sunpreet Singh Rathor
 */

package mode;

public class FileMode extends Mode{
    private String fname;   // Holds the file name to be executed

    public FileMode() {
        super();    // Call to the super class constructor

        fname = null;
    }

    // Method to set fname variable
    public void setFile(String fname) {
        this.fname = fname;
    }

    public void startMode() {
        // File mode
        in.setCode(fh.getByteArray(fname));
        // Start processing the file
        in.processCode();
    }
}
