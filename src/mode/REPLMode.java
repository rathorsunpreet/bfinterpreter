// This file is part of BFInterpreter.
// Copyright (C) 2017  Sunpreet Singh Rathor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * REPLMode.java
 * This is place for REPL style coding for the Brainfuck language.
 * Created by: Sunpreet Singh Rathor
 */

package mode;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class REPLMode extends ExtendedMode{
    private ArrayList<String> history;  // Stores all Bf statements
    private String text;    // Intermediary for storing commands/statements
    private boolean run;

    // For methods
    int line1, line2;
    String s,d;

    public REPLMode() {
        super();

        history = new ArrayList<String>();
        text = null;
        run = true;

        line1 = -1;
        line2 = -1;
        s = null;
        d = null;
    }

    protected void processCommand() {
        String[] subparts = text.split(" ");
        Class<?> cls = this.getClass();
        Method m = null;
        try {
            if(subparts.length == 1) {
                m = cls.getDeclaredMethod(text);
                m.invoke(this);
            } else if(subparts.length == 2) {
                m = cls.getDeclaredMethod(subparts[0], String.class);
                m.invoke(this, subparts[1]);
            } else if(subparts.length == 3){
                m = cls.getDeclaredMethod(subparts[0], String.class, String.class);
                m.invoke(this, subparts[1], subparts[2]);
            } else {
                m = cls.getDeclaredMethod(subparts[0], String.class, String.class, String.class);
                m.invoke(this, subparts[1], subparts[2], subparts[3]);
            }
        } catch (NoSuchMethodException e) {
            //e.printStackTrace();
            uh.displayStringNLine("Method \"" + text +"\" does not exist. Type help to get a list of available methods.");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startMode() {
        while(true) {
            // By default keep checking for commands
            // If not a command then pass input to interpreter
            text = uh.getString("\nInput: ");
            // change all text inputs to lower case
            text = text.toLowerCase();
            if(checkCommmand())
                processCommand();
            else {
                if(run == true) {
                    history.add(text);
                    String code = "";
                    for (String str : history) {
                        code = code.concat(str);
                    }
                    in.reset();
                    in.setCode(code.getBytes());
                    in.processCode();
                }
            }
        }
    }

    /**
     * Methods to check validity of input
     */

    // Method checks if entered text is a valid command or not
    private boolean checkCommmand() {
        r = Pattern.compile("[\\[\\]\\+\\.,-^0]");
        m = r.matcher(text);
        return !m.lookingAt();
    }

    // Method to check if given string represents a text file
    public boolean checkName(String fname) {
        return fname.contains(".txt");
    }

    // Method to check if given string is boolean value
    public boolean checkIfBoolean(String val) {
        r = Pattern.compile("[tT][rR][uU][eE]");
        m = r.matcher(val);
        Pattern p = Pattern.compile("[fF][aA][lL][sS][eE]");
        Matcher f = p.matcher(val);
        return (m.find() || f.find());
    }

    public boolean checkIfInRange(int n1, int size) {
        if((n1 <= size) && (n1 != 0))
            return true;
        uh.displayStringNLine(n1 + " is not within range.");
        return false;
    }

    /**
     * Interact mode commands
     */

    // Help method
    public void help() {
        uh.displayStringNLine("");
        uh.displayStringNLine("BFInterpreter  Copyright (C) 2017  Sunpreet Singh Rathor\n" +
                "    This program comes with ABSOLUTELY NO WARRANTY; for details type `show_w'.\n" +
                "    This is free software, and you are welcome to redistribute it\n" +
                "    under certain conditions; type `show_c' for details.");
        uh.displayStringNLine("");
        uh.displayStringNLine("This command shows list of commands with short descriptions.");
        uh.displayStringNLine("-------------------------------------------------");
        uh.displayStringNLine("Command [optional args] - Description");
        uh.displayStringNLine("");
        uh.displayStringNLine("1) show_history - Lists all previously entered BF statements.");
        uh.displayStringNLine("2) save_current [filename]- Saves current history into a file named history.txt, otherwise save history into specified filename. If file exists, it is overwritten.");
        uh.displayStringNLine("3) load_history [filename] - Loads history from history.txt, or from specified filename");
        uh.displayStringNLine("4) clear_history - Deletes all current statements.");
        uh.displayStringNLine("5) delete_history [filename] - Deletes stored history in history.txt or specified filename.");
        uh.displayStringNLine("6) reset_interpreter - Resets the interpreter and history is not run.");
        uh.displayStringNLine("7) rerun - Runs all statements entered upto this point again.");
        uh.displayStringNLine("8) set_run boolean - Sets if statements entered should be executed or not.");
        uh.displayStringNLine("9) copy_line n1 [r1] [n2] - Copy line n1, r1 times at line n2. If n2 is not specified copy occurs at recent unoccupied index of history. If r1 is not specified, line n1 is copied exactly once.");
        uh.displayStringNLine("10) move_line n1 [n2] - Moves line n1 to n2. If n2 is not specified move occurs at recent unoccupied index of history.");
        uh.displayStringNLine("11) switch_line n1 [n2] - Switches line n1 with n2. If n2 is not specified move occurs at recent unoccupied index of history.");
        uh.displayStringNLine("12) delete_line [n1] - Deletes line n1. If n1 is not specified, last index of history is deleted.");
        uh.displayStringNLine("13) show_cells - Display all used memory cells.");
        uh.displayStringNLine("14) exit - Quits application");
    }

    // show_w method
    public void show_w() {
        uh.displayStringNLine(" 15. Disclaimer of Warranty.\n" +
                "\n" +
                "  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY\n" +
                "APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT\n" +
                "HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY\n" +
                "OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,\n" +
                "THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR\n" +
                "PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM\n" +
                "IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF\n" +
                "ALL NECESSARY SERVICING, REPAIR OR CORRECTION.");
    }

    // show_c method
    public void show_c() {
        if(fh.setup()) {
            while (fh.hasNext()) {
                uh.displayStringNLine(fh.getCopyright());
            }
            fh.closeFile();
        } else {
            uh.displayStringNLine("File COPYING cannot be located. Please find it at project site or <http://www.gnu.org/licenses/>");
        }
    }

    //show_history method
    public void show_history() {
        for(int i = 0; i < history.size(); i++) {
            uh.displayStringNLine(i+1 + ") " + history.get(i));
        }
    }

    // save_current method with a parameter
    public void save_current(String fname) {
        if(checkName(fname)) {
            fh.writeArrayList(history, fname);
            uh.displayStringNLine("File " + fname + " write successful.");
        } else
            uh.displayStringNLine("\""+fname + "\" is not a valid text file name.");
    }

    // save_current method without parameter
    public void save_current() {
        fh.writeArrayList(history, null);
    }

    // load_history method with a pararmeter
    public void load_history(String fname) {
        if(fh.checkIfFile(fname)) {
            history = fh.readArrayList(fname);
            uh.displayStringNLine("History successfully loaded. Commencing rerun of new history.");
            rerun();
        } else {
           uh.displayStringNLine("Filename " + fname + " does not exist or is not a file.");
        }
    }

    // load_history method without pararmeter
    public void load_history() {
        history = fh.readArrayList(null);
        uh.displayStringNLine("History successfully loaded. Commencing rerun of new history.");
        rerun();
    }

    // clear_history method
    public void clear_history() {
        history.clear();
        uh.displayStringNLine("History cleared.");
    }

    // delete_history with a parameter
    public void delete_history(String fname) {
        if(fh.checkIfFile(fname)) {
            if(fh.deleteFile(fname))
                uh.displayStringNLine("Filename " + fname + " deleted successfully!");
            else
                uh.displayStringNLine("Filename " + fname + " cannot be deleted!");
        } else {
            uh.displayStringNLine("Filename " + fname + " does not exist or is not a file.");
        }
    }

    // delete_history with a parameter
    public void delete_history() {
        if(fh.checkIfFile(null)) {
            if(fh.deleteFile(null))
                uh.displayStringNLine("Filename history.txt deleted successfully!");
            else
                uh.displayStringNLine("Filename history.txt cannot be deleted!");
        } else {
            uh.displayStringNLine("Filename history.txt does not exist or is not a file.");
        }
    }

    // reset_interpreter method
    public void reset_interpreter() {
        uh.displayStringNLine("Interpreter reset successful.");
        in.reset();
    }

    // rerun method
    public void rerun() {
        uh.displayStringNLine("Rerunning history.");
        in.reset();
        String a = "";
        for(String s : history) {
            a = a.concat(s);
        }
        in.setCode(a.getBytes());
        in.processCode();
    }

    // set_run method
    public void set_run(String val) {
        if(checkIfBoolean(val))
            run = Boolean.parseBoolean(val);
        else
            uh.displayStringNLine("\"" + val + "\" is not a valid boolean value.");
    }

    // copy_line method with three parameters
    public void copy_line(String n1, String r1, String n2) {
        if(checkIfNumber(n1, history.size()) && checkIfPostiveInteger(r1) && checkIfNumber(n2, history.size())) {
            int times = Integer.parseInt(r1);
            line1 = Integer.parseInt(n1);
            line2 = Integer.parseInt(n2);
            s = history.get(line1 - 1);

            for (int i = 0; i < times; i++)
                history.add(line2 - 1, s);

            uh.displayStringNLine(n1 + " has been copied to " + n2 + " " + r1 + " times!");
        }
    }

    // copy_line method with two line numbers
    public void copy_line(String n1, String n2) {
        if(checkIfNumber(n1, history.size())) {
            line1 = Integer.parseInt(n1);
            line2 = Integer.parseInt(n2);
            s = history.get(line1 - 1);
            // Check if second number is a line number or not
            String result = uh.getString("\nIs the second number a line number? [yes/no] ");
            if (result.equalsIgnoreCase("yes") || result.equalsIgnoreCase("y")) {
                if(checkIfNumber(n2, history.size()))
                    history.add(line2 - 1, s);
            } else {
                for (int i = 0; i < line2; i++)
                    history.add(s);
            }

            uh.displayStringNLine("Method successful!");
        }
    }

    // copy_line method with one parameter
    public void copy_line(String n1) {
        if(checkIfNumber(n1, history.size())) {
            line1 = Integer.parseInt(n1);
            s = history.get(line1);
            history.add(s);
            uh.displayStringNLine("Method successful!");
        }
    }

    // move_line method with two parameters
    public void move_line(String n1, String n2) {
        if(checkIfNumber(n1, history.size()) && checkIfNumber(n2, history.size())) {
            line1 = Integer.parseInt(n1);
            line2 = Integer.parseInt(n2);
            s = history.get(line1 - 1);
            if (line1 < line2) {
                history.add(line2 - 1, s);
                history.remove(line1 - 1);
            } else {
                history.add(line2 - 1, s);
                history.remove(line1);
            }

            uh.displayStringNLine("Method successful!");
        }
    }

    // move_line method with one parameter
    public void move_line(String n1) {
        if(checkIfNumber(n1, history.size())) {
            line1 = Integer.parseInt(n1);
            line2 = history.size();
            s = history.get(line1 - 1);
            history.add(line2, s);
            history.remove(line1 - 1);

            uh.displayStringNLine("Method successful!");
        }
    }

    // switch_line method with two parameters
    public void switch_line(String n1, String n2) {
        if(checkIfNumber(n1, history.size()) && checkIfNumber(n2, history.size())) {
            line1 = Integer.parseInt(n1);
            line2 = Integer.parseInt(n2);
            s = history.get(line1 - 1);
            d = history.get(line2 - 1);

            if (line1 < line2) {
                history.remove(line1 - 1);
                history.remove(line2 - 2);

                history.add(line2 - 2, s);
                history.add(line1 - 1, d);
            } else {
                history.remove(line1 - 1);
                history.remove(line2 - 1);

                history.add(line2 - 1, s);
                history.add(line1 - 1, d);
            }

            uh.displayStringNLine("Method successful!");
        }
    }

    // switcth_line method with one parameter
    public void switch_line(String n1) {
        if(checkIfNumber(n1, history.size())) {
            line1 = Integer.parseInt(n1);
            line2 = history.size();
            s = history.get(line1 - 1);
            d = history.get(line2 - 1);

            history.remove(line1 - 1);
            history.remove(line2 - 2);

            history.add(line2 - 2, s);
            history.add(line1 - 1, d);

            uh.displayStringNLine("Method successful!");
        }
    }

    // delete_line method with one parameter
    public void delete_line(String n1) {
        if(checkIfNumber(n1, history.size())) {
            history.remove(Integer.parseInt(n1));
            uh.displayStringNLine(n1 + "has been deleted!");
        }
    }

    // delete_line method without a parameter
    public void delete_line() {
        history.remove(history.size());
        uh.displayStringNLine("All history has been deleted!");
    }

    // show_cells method
    public void show_cells() {
        in.showCells();
    }

    // exit method
    public void exit() {
        System.exit(0);
    }

}
