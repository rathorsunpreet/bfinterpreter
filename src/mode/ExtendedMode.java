// This file is part of BFInterpreter.
// Copyright (C) 2017  Sunpreet Singh Rathor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * ExtendedMode.java
 * This is the abstract class which extends Mode class and adds new methods.
 * This class is used as Superclass for DebugMode and REPLMode classes.
 * Created by: Sunpreet Singh Rathor
 */

package mode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class ExtendedMode extends Mode{
    protected Pattern r;
    protected Matcher m;

    public ExtendedMode() {
        r = null;
        m = null;
    }

    public abstract boolean checkIfInRange(int n1, int size);

    protected abstract void processCommand();

    // Method to check if given string is positive integer
    // Checks if String contains anything except positive integer and return inverse value
    public boolean checkIfPostiveInteger(String n1) {
        r = Pattern.compile(".*\\D.*");
        m = r.matcher(n1);
        if(!m.find() == false)
            uh.displayStringNLine(n1 + " is not a positive integer.");
        return !m.find();
    }

    // Method to check if n1 is a positive number and within the range specified by 0/1 and size
    public boolean checkIfNumber(String n1, int size) {
        if(checkIfPostiveInteger(n1) && checkIfInRange(Integer.parseInt(n1), size))
            return true;
        return false;
    }
}
