// This file is part of BFInterpreter.
// Copyright (C) 2017  Sunpreet Singh Rathor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * FileHandler.java
 * This is the basic abstract superclass. It contains all the necessary components to work with the interpreter, user and disk.
 * This is the superclass for FileMode and ExtendedMode classes.
 * Created by: Sunpreet Singh Rathor
 */

package mode;

import handler.FileHandler;
import handler.UserHandler;
import interpreter.Interpreter;

public abstract class Mode {
    protected FileHandler fh;
    protected UserHandler uh;
    protected Interpreter in;

    public Mode() {
        fh = new FileHandler();
        uh = new UserHandler();
        in = new Interpreter(uh);
    }

    public abstract void startMode();
}
