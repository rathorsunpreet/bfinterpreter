// This file is part of BFInterpreter.
// Copyright (C) 2017  Sunpreet Singh Rathor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * DebugMode.java
 * This is where the debug mode for the specified file takes place.
 * Created by: Sunpreet Singh Rathor
 */

package mode;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;


public class DebugMode extends ExtendedMode{

    private byte[] code;    // File Code goes here
    private int ptr;    // Pointer to code array. ptr points to keyword that will be executed next but has not been executed now.
    private String text;    // This holds commands entered by the user

    private ArrayList<Integer> breakpoints; // Stores all breakpoints

    private String fname;

    public DebugMode() {
        super();

        code = null;
        ptr = 0;
        text = null;

        breakpoints = new ArrayList<Integer>();

        fname = null;
    }

    public void setFile(String fname) {
        this.fname = fname;
    }

    public void startMode() {
        code = fh.getByteArray(fname);
        while(true) {
            text = uh.getString("\nCommand: ");
            processCommand();
        }
    }

    protected void processCommand() {
        String[] subparts = text.split(" ");
        Class cls = this.getClass();
        Method m = null;
        try {
            if(subparts.length == 1) {
                m = cls.getDeclaredMethod(text);
                m.invoke(this);
            } else if(subparts.length == 2) {
                m = cls.getDeclaredMethod(subparts[0], String.class);
                m.invoke(this, subparts[1]);
            }
        } catch (NoSuchMethodException e) {
            //e.printStackTrace();
            uh.displayStringNLine("Method \"" + text +"\" does not exist. Type help to get a list of available nethods.");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Validation methods
     */
    public boolean checkIfInRange(int n1, int size) {
        if((n1 <= size) && (n1 >= 0))
            return true;
        uh.displayStringNLine(n1 + " is not within range.");
        return false;
    }

    /**
     * Debug mode methods
     */

    // help method
    public void help() {
        uh.displayStringNLine("");
        uh.displayStringNLine("BFInterpreter  Copyright (C) 2017  Sunpreet Singh Rathor\n" +
                "    This program comes with ABSOLUTELY NO WARRANTY; for details type `show_w'.\n" +
                "    This is free software, and you are welcome to redistribute it\n" +
                "    under certain conditions; type `show_c' for details.");
        uh.displayStringNLine("");
        uh.displayStringNLine("This command shows list of commands with short descriptions.");
        uh.displayStringNLine("-------------------------------------------------");
        uh.displayStringNLine("Command [optional args] - Description");
        uh.displayStringNLine("1) show_current_position - Shows current position of pointer.");
        uh.displayStringNLine("2) set_breakpoint n1 - Sets breakpoint at keyword n1.");
        uh.displayStringNLine("3) remove_breakpoint n1 - Removes breakpoint at keyword n1 if present else does nothing.");
        uh.displayStringNLine("4) show_all_breakpoints - Shows all breakpoints currently entered.");
        uh.displayStringNLine("5) remove_all_breakpoints - Removes all breakpoints.");
        uh.displayStringNLine("6) continue_till_breakpoint - Run code till breakpoint encountered. If no breakpoints are present then simply runs all the code without stopping.");
        uh.displayStringNLine("7) jump_to_keyword n1 - Jump to keyword n1 inside code.");
        uh.displayStringNLine("8) show_tape - Shows current values of the memory tape which are not zero.");
        uh.displayStringNLine("9) reset_interpreter - Resets interpreter.");
        uh.displayStringNLine("10) reset_debugger - Resets the debugger including breakpoints and the interpreter but the code remains intact.");
        uh.displayStringNLine("11) previous - Go to previous keyword.");
        uh.displayStringNLine("12) next - Go to next keyword.");
        uh.displayStringNLine("13) force_exit_loop - Forcefully jumps out of loop if inside one. Does not complete the full iterations of the said loop.");
        uh.displayStringNLine("14) show_code - Shows code on console.");
        uh.displayStringNLine("15) run_all - Runs all code and does not stop for breakpoints.");
        uh.displayStringNLine("16) exit - Quits debugger.");
    }

    // show_w method
    public void show_w() {
        uh.displayStringNLine(" 15. Disclaimer of Warranty.\n" +
                "\n" +
                "  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY\n" +
                "APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT\n" +
                "HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY\n" +
                "OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,\n" +
                "THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR\n" +
                "PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM\n" +
                "IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF\n" +
                "ALL NECESSARY SERVICING, REPAIR OR CORRECTION.");
    }

    // show_c method
    public void show_c() {
        if(fh.setup()) {
            while (fh.hasNext()) {
                uh.displayStringNLine(fh.getCopyright());
            }
            fh.closeFile();
        } else {
            uh.displayStringNLine("File COPYING cannot be located. Please find it at project site or <http://www.gnu.org/licenses/>");
        }
    }

    // show_current_position method
    public void show_current_position() {
        uh.displayStringNLine("Pointer Location - Current Keyword");
        uh.displayString("" + ptr + "/");
        if(ptr <= (code.length - 1) && ptr != -1)
            uh.displayByteAsChar(code[ptr]);
        uh.displayString("\n");
    }

    // set_breakpoint method
    public void set_breakpoint(String n1) {
        if(checkIfNumber(n1, code.length)) {
            breakpoints.add(Integer.parseInt(n1) - 1);
            uh.displayStringNLine("Breakpoint added at keyword " + n1);
        } else {
            uh.displayStringNLine(n1 + " is either too small a number or too large a number for the current code!");
        }
    }

    // remove_breakpoint method
    public void remove_breakpoint(String n1) {
        if(breakpoints.contains((int)Integer.parseInt(n1)-1)) {
            breakpoints.remove((Integer) Integer.parseInt(n1) - 1);
            uh.displayStringNLine("Breakpoint removed from keyword " + n1);
        } else {
            uh.displayStringNLine(n1 + " does not exists in breakpoint list!");
        }
    }

    // show_all_breakpoint method
    public void show_all_breakpoints() {
        if(breakpoints.isEmpty()) {
            uh.displayStringNLine("No current breakpoints exist!");
        } else {
            uh.displayStringNLine("Breakpoints are: ");
            uh.displayStringNLine("Breakpoint number - Keyword - Surrounding Code");
            uh.displayStringNLine("-------------------------");
            for (int i = 0; i < breakpoints.size(); i++) {
                int n1 = breakpoints.get(i);
                if (n1 == 0) {
                    uh.displayString("" + n1 + 1 + "/");
                    uh.displayByteAsChar(code[n1]);
                    uh.displayString("/");
                    uh.displayByteAsChar(code[n1]);
                    uh.displayString(" \"");
                    uh.displayByteAsChar(code[n1 + 1]);
                    uh.displayString("\" ");
                    uh.displayByteAsChar(code[n1 + 2]);
                    uh.displayString("\n");
                } else if (n1 == code.length - 1) {
                    uh.displayString("" + n1 + 1 + "/");
                    uh.displayByteAsChar(code[n1]);
                    uh.displayString("/");
                    uh.displayByteAsChar(code[n1 - 2]);
                    uh.displayString(" \"");
                    uh.displayByteAsChar(code[n1 - 1]);
                    uh.displayString("\" ");
                    uh.displayByteAsChar(code[n1]);
                    uh.displayString("\n");
                } else {
                    uh.displayString("" + n1 + 1 + "/");
                    uh.displayByteAsChar(code[n1]);
                    uh.displayString("/");
                    uh.displayByteAsChar(code[n1 - 1]);
                    uh.displayString(" \"");
                    uh.displayByteAsChar(code[n1]);
                    uh.displayString("\" ");
                    uh.displayByteAsChar(code[n1 + 1]);
                    uh.displayString("\n");
                }
            }
        }
    }

    // remove_all_breakpoint method
    public void remove_all_breakpoints() {
        breakpoints.clear();
        uh.displayStringNLine("All breakpoints removed!");
    }

    // continue_till_breakpoint method
    public void continue_till_breakpoint() {
        // Sort the ArrayList breakpoints
        Collections.sort(breakpoints);

        for(;ptr < code.length ; ptr++) {
            if(breakpoints.contains((Integer) ptr)) {
                break;
            }
            in.applyRules(code[ptr]);
        }
    }

    // previous method
    public void previous() {
        if(ptr <= 0) {
            uh.displayStringNLine("Current Pointer at location from which going back is not possible!");
        } else {
            int prev = ptr - 1;
            in.reset();
            for (int i = 0; i < prev; i++) {
                uh.displayStringNLine(code[i] + "");
                in.applyRules(code[i]);
                if (in.getIsInLoop()) {
                    i = in.getI();
                }
            }
        }
    }

    // next method
    public void next() {
        if(ptr <= (code.length - 1)) {
            if(ptr == -1) {
                ptr++;
            } else {
                in.applyRules(code[ptr]);
                if (in.getIsInLoop()) {
                    ptr = in.getI();
                } else {
                    ptr++;
                }
                uh.displayStringNLine(code[ptr] + "");
            }
        } else {
            uh.displayStringNLine(" Pointer is at the end of file!");
        }
    }

    // show_code method
    public void show_code() {
        for(int i = 0; i < code.length; i++)
            uh.displayByteAsChar(code[i]);
        uh.displayString("\n");
    }

    // show_tape method
    public void show_tape() {
        in.showCells();
    }

    // run_all method
    public void run_all() {
        in.setCode(code.clone());
        in.processCode();
        ptr = code.length;
    }

    // exit method
    public void exit() {
        System.exit(0);
    }

    // reset_interpreter method
    public void reset_interpreter() {
        in.reset();
        uh.displayStringNLine("Interpreter reset complete!");
    }

    // reset_debugger method
    public void reset_debugger() {
        ptr = 0;
        remove_all_breakpoints();
        in.reset();
        uh.displayStringNLine("Debugger reset complete!");
    }

    // jump_to_keyword method
    public void jump_to_keyword(String n1) {
        if(checkIfNumber(n1, code.length)) {
            int line1 = Integer.parseInt(n1);
          if (line1 < ptr) {
                in.reset();
                if(line1 == 0) {
                    ptr = -1;
                } else {
                    for (int i = 0; i < line1; i++) {
                        in.applyRules(code[i]);
                        uh.displayByteAsChar(code[i]);
                        if (in.getIsInLoop()) {
                            i = in.getI();
                        }
                        ptr = i;
                    }
                    ptr++;
                }
                uh.displayString("\n");
            } else if (line1 > ptr) {
                for (int i = ptr; i < line1; i++) {
                    in.applyRules(code[i]);
                    uh.displayByteAsChar(code[i]);
                    if (in.getIsInLoop()) {
                        i = in.getI();
                    }
                    ptr = i;
                }
                ptr++;
              uh.displayString("\n");
            }
        } else {
            uh.displayStringNLine(n1+ " is either too big or too small for the current code!");
        }
    }

    // force_exit_loop method
    public void force_exit_loop() {
        // Check if at beginning of loop or somewhere inside the loop
        if(code[ptr] == 91 || in.checkIfInLoop()) {
            // Loop from ptr till you reach ] keyword
            for(int i = ptr; code[i] != 93; i++) {
                in.applyRules(code[i]);
            }
        }

        // Check if at ] keyword
        if(code[ptr] == 93) {
            ptr++;
            if(!checkIfInRange(ptr, code.length)) {
                ptr = code.length;
            }
            in.setCurrentTapeValue((byte)0);
        }
    }
}
