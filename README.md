# BFInterpreter #

- - - -

BFInterpreterv1.1 was made by Sunpreet Singh Rathor. This project is under the GNU GPL v3.  
  
This project is an interpreter for the esoteric programming language **Brainfuck**, details can be found at [Brainfuck Wiki](http://esolangs.org/wiki/brainfuck).  

- - - -
## Update Log ##
1. BFInterpreterv1.0 was released as an Eclipse Project.
2. Project was updated to Ant build with instructions on how to build the project (v1.1).

- - - -
## Directory Structure ##
<pre>
Root
|
|----- src (Contains all source files)
|----- build\* (Contains all class files upon compilation)
|----- dist\* (Contains the executable jar)
|----- logs\* (Contains the logs for execution of each ant <target> command except clean)
</pre>
\* These folders would only be made when the appropriate ant commands are executed.

- - - -
## Ant Build ##

The project has been updated from Eclipse Build to Apache Ant build. The following Ant targets are available:

1. init - Creates the directories used by other targets, also deletes the clean log file if present
2. compile - Compiles all sources files
3. dist - Creates the executable JAR file
4. clean - Removes all the class files and other folders leaving behind a clean log file
5. run - Executes the jar with --h as parameter
6. info - Provides this stated list and can simply be called by executing *ant* or *ant info* .

To build the project simply use the command:
<pre>
`ant dist`
</pre>
and then follow **Usage Instructions**.

- - - -
## Modes ##

The project has the following three modes which can be chosen at run-time using command-line options:  

1. **File Mode**:  
This mode allows the user to write all their Brainfuck code in a file and run it.  
  
2. **REPL Mode**:  
This mode is an REPL Style coding platform. With this the user can run Brainfuck statements directly on the application without needing to make a file. This mode has it's own set of commands, anything within [] is optional:  
	a. `show_history` - Lists all previously entered BF statements.  
	b. `save_current [filename]`- Saves current history into a file named history.txt, otherwise save history into specified filename. If file exists, it is overwritten.  
	c. `load_history [filename]` - Loads history from history.txt, or from specified filename  
	d. `clear_history` - Deletes all current statements.  
	e. `delete_history [filename]` - Deletes stored history in history.txt or specified filename.  
	f. `reset_interpreter` - Resets the interpreter and history is not run.  
	g. `rerun` - Runs all statements entered upto this point again.  
	h. `set_run boolean` - Sets if statements entered should be executed or not.  
	i. `copy_line n1 [r1] [n2]` - Copy line n1, r1 times at line n2. If n2 is not specified copy occurs at recent unoccupied index of history. If r1 is not specified, line n1 is copied exactly once.  
	j. `move_line n1 [n2]` - Moves line n1 to n2. If n2 is not specified move occurs at recent unoccupied index of history.  
	k. `switch_line n1 [n2]` - Switches line n1 with n2. If n2 is not specified move occurs at recent unoccupied index of history.  
	l. `delete_line [n1]` - Deletes line n1. If n1 is not specified, last index of history is deleted.  
	m. `show_cells` - Display all used memory cells.  
	n. `exit` - Quits application  
The user can exit this mode by typing 0 (zero) or exit (any variation).  
  
3. **Debug Mode**:   
With Debug Mode, the user runs the file manually and can also check the values of the interpreter. This mode also comes with it's own set of statements, anything between [] is optional:  
	a. `show_current_position` - Shows current position of pointer.  
	b. `set_breakpoint n1` - Sets breakpoint at keyword n1.  
	c. `remove_breakpoint n1` - Removes breakpoint at keyword n1 if present else does nothing.  
	d. `show_all_breakpoints` - Shows all breakpoints currently entered.  
	e. `remove_all_breakpoints` - Removes all breakpoints.  
	f. `continue_till_breakpoint` - Run code till breakpoint encountered. If no breakpoints are present then simply runs all the code without stopping.  
	g. `jump_to_keyword n1` - Jump to keyword n1 inside code.  
	h. `show_tape` - Shows current values of the memory tape which are not zero.  
	i. `reset_interpreter` - Resets interpreter.  
	j. `reset_debugger` - Resets the debugger including breakpoints and the interpreter but the code remains intact.  
	k. `previous` - Go to previous keyword.  
	l. `next` - Go to next keyword.  
	m. `force_exit_loop` - Forcefully jumps out of loop if inside one. Does not complete the full iterations of the said loop.  
	n. `show_code` - Shows code on console.  
	o. `run_all` - Runs all code and does not stop for breakpoints.  
	p. `exit` - Quits debugger.  
The user can type exit (any variation) to quit the debugger.  

- - - -
## Usage Instructions ##

  
The following command-line arguments are available to the user:  
1. `--h/--help`: For printing this help.  
2. `--r/--repl-mode`: For a REPL style interpreter.  
3. `--f/--file`: Specify a full filename after this option.  
4. `--d/--debug-mode`: Specify a full filename after this option and debug the program.  
  
**USAGE EXAMPLE**:  
If you are using an IDE to run the code, then apply the command-line arguments appropriately.  
   
If you are using the jar file, then use the following examples in a terminal or command prompt:

1. **For File/Debug Mode**:  
`java -jar bfinterpreter-<date>.jar --f/--file-mode/--d/--debug-mode filename.txt`  

2. **For REPL Mode**:  
`java -jar bfinterpreter-<date>.jar --r/--repl-mode`

- - - -
**NOTE**:  
If you are using a file to work with the interpreter, then make sure the file is a text file or the extension of the file is ".txt".  

- - - -
**CONTACT**:  
If you feel that I have missed something or you added something on your own, then please contact me at rathorsunpreetsingh@gmail.com .  
  
- - - -
**TO-DO**:  
1. Improve code.
